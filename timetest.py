import time
import requests
import urllib3

#weburl = "https://www.googleapis.com/books/v1/volumes?q=isbn:0747532699"
weburl = "https://bing.com/covid/data"
loopCount = 50


def test_urllib3():
	addy = urllib3.PoolManager()
	response = addy.request('GET', weburl)
	response.status
	return response.data

def test_requests():
	response = requests.get(weburl)
	response.status_code
	return response.content

def main():
	url3Total = 0
	reqTotal = 0

	for i in range(loopCount):
		tic = time.perf_counter()
		time_req = test_urllib3()
		toc = time.perf_counter()
		url3Total += (toc - tic)
		print(f"URL3: {i}")
	url3Avg = url3Total / loopCount

	for i in range(loopCount):
		tic = time.perf_counter()
		time_req = test_requests()
		toc = time.perf_counter()
		reqTotal += (toc - tic)
		print(f"REQ: {i}")
	reqAvg = reqTotal / loopCount

	print(f"After {loopCount} runs, these are the average download times for the following modules:")
	print(f"URLLIB3 averge download is: {url3Avg:0.4f} seconds")
	print(f"REQUEST averge download is: {reqAvg:0.4f} seconds")

main()