# Module Speed Test

Quick test to see which python module downloads URLs faster, URLLIB3 or REQUESTS. 
  
On smaller data set:
```
After 100 runs, these are the average download times for the following modules:
URLLIB3 averge download is: 0.413770 seconds
REQUEST averge download is: 0.412642 seconds
```
 
On larger data set:
```
After 50 runs, these are the average download times for the following modules:
URLLIB3 averge download is: 0.579423 seconds
REQUEST averge download is: 0.318027 seconds
```
